import requests
from bs4 import BeautifulSoup
import html5lib
import pandas as pd
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pyvirtualdisplay import Display

product_url_list = []
records = []

# grabbing products urls for catalogue


def url_grab(page):
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.get(
        'https://www.petsonic.com/snacks-huesos-para-perros/?p={}'.format(page))
    driver.set_window_position(-10000, 0)
    driver.set_window_size(800, 800)
    response = driver.page_source
    soup = BeautifulSoup(response, 'lxml')
    # finding all <a> tags which contains name and product link
    results = soup.find_all('a', attrs={'class': 'product-name'})
    i = 0
    while(i != len(results)):
        product_url_list.append(results[i]['href'])  # grabbing urls in list
        i += 1
    driver.quit()
    return product_url_list

# grabbing info about product


def info_grab():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.set_window_position(-10000, 0)
    driver.set_window_size(800, 800)
    for list_id in range(257):  # getting urls one by one and finding info
        weight_list = []
        price_list = []
        driver.get(product_url_list[list_id])
        product = driver.page_source
        soup = BeautifulSoup(product, 'lxml')
        # finding <ul> tag which contains info about weight and price
        for ultag in soup.find_all('ul', attrs={'class', 'attribute_radio_list'}):
            
            # getting weight from list
            for litag_weight in ultag.find_all('span', attrs={'class', 'radio_label'}):
                weight_list.append(litag_weight.text)
                
                # getting price from list
                for litag_price in ultag.find_all('span', attrs={'class', 'price_comb'}):
                    price_list.append(litag_price.text)

        # collecting all info that we need and pack it in list of tuples
        for tag_id in range(len(weight_list)):
            title_and_weight = soup.find(
                'p', attrs={'class': 'product_main_name'}).text[1:-6] + ' - ' + weight_list[tag_id]
            price = price_list[tag_id]
            picture = soup.find('img', attrs={'id': 'bigpic'})['src']
            records.append((title_and_weight, price, picture))
    driver.quit()
    return records


for page_number in range(12):
    url_grab(page_number)

url_num = 0
while url_num != len(product_url_list):
    info_grab()
    url_num += 1

table = pd.DataFrame(records, columns=[
                     'Title and weight', 'price', 'picture source'])  # creating table using pandas
table.to_csv('petsonic.csv', index=False, encoding='utf-8',
             sep=',')  # converting table to csv
